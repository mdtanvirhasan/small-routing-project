import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FifthComponent } from './fifth/fifth.component';
import { FirstComponent } from './first/first.component';
import { FourthComponent } from './fourth/fourth.component';
import { SecondComponent } from './second/second.component';
import { ThirdComponent } from './third/third.component';
const routes: Routes = [
  {path:'first-component', component:FirstComponent},
  {path:'second-component',component:SecondComponent},
  {path:'third-component',component:ThirdComponent},
  {path:'fourth-coponent',component:FourthComponent},
  {path:'fifth-copoonent',component:FifthComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
